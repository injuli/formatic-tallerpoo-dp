<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Alien
 *
 * @author pabhoz
 */
abstract class Alien {

    private $nombre;
    private $edad;
    private $especie;
    private $origen;
    private $moral;
    
    const COMUNICACION='telepaticamente';


    function __construct($nombre, $edad, $especie, $origen, $moral="neutral") {
        $this->nombre = $nombre;
        $this->edad = $edad;
        $this->especie = $especie;
        $this->origen = $origen;
        $this->moral = $moral;
    }
    function getNombre() {
        return $this->nombre;
    }

    function getEdad() {
        return $this->edad;
    }

    function getEspecie() {
        return $this->especie;
    }

    function getOrigen() {
        return $this->origen;
    }

    function getMoral() {
        return $this->moral;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setEdad($edad) {
        $this->edad = $edad;
    }

    function setEspecie($especie) {
        $this->especie = $especie;
    }

    function setOrigen($origen) {
        $this->origen = $origen;
    }

    function setMoral($moral) {
        $this->moral = $moral;
    }
    
    abstract public function interact();
    public function whoIAm(){
        return Alien::COMUNICACION."dice mi nombre es:".$this->getNombre().", "
                . "del planeta ". $this->getOrigen().", soy un".$this->getEspecie().
                " y soy ". $this->getMoral();
    }
    
}
